# piX


## Auteurs
A personal project managed by : 
- Manfred Madelaine :fire:

## Description
This project is an attempt to create an artificial intelligence capable to play 
and win a Connect Four like game (Puissance 4 in French) against : 
- a Human
- an AI playing randomly
- an AI capable to attack
- an AI using minimax algorithm

It will try to connect X pieces on an M x N board.  

## Quick Start
- import the project
- go to the repository downloaded in a terminal
- execute the following command : __python3 p4.py__